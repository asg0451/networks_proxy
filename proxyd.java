import java.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.*;
import java.util.regex.*;
import java.time.*;
import java.time.temporal.*;

/**
 * The proxyd class makes a ServerSocket which waits for a client connection.
 * When it gets one, an instance of RequestHandler is spawned,
 * and the cycle starts again.
 */
public class proxyd {
     private ServerSocket listener;
     public static void main(String[] args) {
          int port = -1;
          if(args.length >= 2 &&
             args[0].equals("-port")) {
               try {
                    port = Integer.parseInt(args[1]);
               } catch(NumberFormatException e) {
                    System.err.println("error: specified port is not a number\nUsage: java proxyd (port 5010 default)\nor: java proxyd -port <portnum>");
                    return;
               }

          } else if(args.length > 0) {
               System.err.println("error. Usage: java proxyd (port 5010 default)\nor: java proxyd -port <portnum>");
               return;
          } else {
               // default port
               port = 5010;
          }

          System.out.println("starting proxy on port " + port);

          new proxyd(port);
     }

     public proxyd(int port) {
          ConcurrentHashMap<String,InetAddress> dnsCache = new ConcurrentHashMap<>();
          ConcurrentHashMap<String,Instant> dnsTimeMap = new ConcurrentHashMap<>();
          DNSCachePruner pruner = new DNSCachePruner(dnsCache, dnsTimeMap);
          pruner.start();

          try {
               listener = new ServerSocket(port);
               while(true) {
                    Socket s = listener.accept();
                    RequestHandler h = new RequestHandler(s, dnsCache, dnsTimeMap);
                    h.start();
               }
          } catch (BindException e) {
               System.err.println("couldn't bind listener socket to port.");
               System.err.println(e);
               e.printStackTrace();
          } catch (Exception e) {
               System.err.println(e);
               e.printStackTrace();
          }
     }

     /**
      * This class does all the work. A new instance of it is spawned
      * for each incoming client request.
      */
     public class RequestHandler extends Thread {
          private Socket socketToClient;
          private Map<String,InetAddress> dnsCache;
          private Map<String,Instant> dnsTimeMap;
          public RequestHandler(Socket s, ConcurrentHashMap<String,InetAddress> dnsCache,
                                ConcurrentHashMap<String,Instant> dnsTimeMap) {
               socketToClient = s;
               this.dnsCache = dnsCache;
               this.dnsTimeMap = dnsTimeMap;
          }

          public void run() {
               try {
                    //*********************** get request data from client, parse it
                    //    (we treat the request header as a string, and the body if any as raw bytes)

                    //************ handle the first line of the request
                    String reqLine = bytestringToString(readSocketLine(socketToClient));

                    // get HTTP verb (get/post/etc)
                    Pattern verbPat = Pattern.compile("\\w+");
                    Matcher verbMatch = verbPat.matcher(reqLine);
                    String  verb = null;
                    if(verbMatch.find()) verb = verbMatch.group(0);

                    // get object url and parse using java's URI class
                    Pattern urlPat = Pattern.compile("^\\w+\\s+(\\S+)");
                    Matcher urlMatch = urlPat.matcher(reqLine);
                    String  url = null;
                    URI uri = null;
                    if(urlMatch.find()) {
                         url = urlMatch.group(1);
                         uri = new URI(url);
                    }

                    if(uri == null || uri.getHost() == null) {
                         System.out.println("error parsing url. dying.");
                         return;
                    }

                    // get HTTP version.
                    Pattern verPat = Pattern.compile("HTTP/(1\\.\\d)\\s*$");
                    Matcher verMatch = verPat.matcher(reqLine);
                    String  ver = null;
                    if(verMatch.find()) ver = verMatch.group(1);

                    int port = (uri.getPort() < 0) ? 80 : uri.getPort();

                    // open socket to the server (DNS resolution courtesy of java InetAddress)
                    // extra credit: try to get ip from dns cache

                    String host = uri.getHost();
                    InetAddress addr = dnsCache.get(host);
                    if(addr == null) {
                         System.out.println("host not in cache. putting addr in map: " + host);
                         addr = InetAddress.getByName(host);
                         dnsCache.put(host, addr);
                         dnsTimeMap.put(host, Instant.now());
                    } else {
                         System.out.println("got dns address from map: " + dnsCache.get(host));
                    }


                    Socket socketToServer = new Socket(addr, port);

                    // open output streams to the client socket and the new server socket
                    DataOutputStream toServer = new DataOutputStream(socketToServer.getOutputStream());
                    DataOutputStream toClient = new DataOutputStream(socketToClient.getOutputStream());

                    // rewrite the top line of the request according to requirements
                    String respTopLine = verb + " " + uri.getPath() +
                         ((uri.getQuery() == null) ? "" : "?" + uri.getQuery())
                         + " HTTP/" + ver + "\r\n";

                    writeString(toServer, respTopLine);

                    //********* handle the rest of the request
                    int contentLength = 0;
                    while(true) {
                         String line = bytestringToString(readSocketLine(socketToClient));

                         // try and find the content-length so we know how long the body is (if it exists)
                         Pattern clPat = Pattern.compile("Content-Length:\\s+([0-9]+)", Pattern.CASE_INSENSITIVE);
                         Matcher clMatch = clPat.matcher(line);
                         if(clMatch.find()) {
                              contentLength = Integer.parseInt(
                                   clMatch.group(1));
                              System.out.println("found content-length: " + contentLength);
                         }

                         // try to apply the required rewrite rules. If one is matched, the output will be written to the stream
                         boolean ruleMatched = false;
                         ruleMatched |= rewriteLineRegex("Proxy-Connection:.*", "Proxy-Connection: close\r\n", toServer, line);
                         ruleMatched |= rewriteLineRegex("Connection:.*", "", toServer, line);

                         // header ends
                         if(line.trim().equals("")) {
                              writeString(toServer, "Connection: close\r\n\r\n");

                              // transmit body data if it exists
                              if(contentLength > 0) {
                                   DataInputStream fromClient = new DataInputStream(socketToClient.getInputStream());
                                   int bytesReadSoFar = 0;
                                   while(true) {
                                        byte [] bl = new byte[4096]; // read in chunks
                                        int bytesRead = fromClient.read(bl);
                                        bytesReadSoFar += bytesRead;
                                        if(bytesRead != -1) {
                                             toServer.write(bl, 0, bytesRead);
                                        }
                                        if(bytesReadSoFar >= contentLength) {
                                             System.out.println("done transmitting req body of size " + bytesReadSoFar);
                                             break;
                                        }
                                   }
                                   toServer.flush();
                              }
                              System.out.println("successfully transmitted req body");
                              // done transmitting this packet
                              break;
                         }
                         // if this line wasn't rewritten or blank, pass it along unchanged
                         else if(!ruleMatched){
                              writeString(toServer, line);
                         }
                    }

                    System.out.println("doing resp");

                    //***************************
                    // done transmitting request, receive response data
                    // and write to client

                    // attempt to read resp data in chunks of 4096 bytes
                    DataInputStream in = new DataInputStream(socketToServer.getInputStream());
                    while(true) {
                         byte [] bl = new byte[4096];
                         int bytesRead = in.read(bl);
                         if(bytesRead < 0) {
                              System.out.println("done transmitting resp");
                              break;
                         }
                         // read can read up to 4096 bytes. this way we transmit only what
                         // was actually read and not uninitialized data
                         toClient.write(bl, 0, bytesRead);
                    }
                    toClient.flush();

                    // we're done. close both sockets.
                    socketToClient.close();
                    socketToServer.close();

               } catch (ConnectException e) {
                    System.err.println("couldn't connect to remote host");
                    System.err.println(e);
                    e.printStackTrace();
               } catch (SocketException e) {
                    System.err.println("socket closed while we were writing");
                    System.err.println(e);
                    e.printStackTrace();
               } catch (Exception e) {
                    System.err.println(e);
                    e.printStackTrace();
               }

          }

          ///*********************** various helper functions

          /**
           * Makes a string from a byte list
           */
          private String bytestringToString(List<Byte> bs) {
               return bs.stream()
                    .map(b -> (char)b.byteValue())
                    .map(c -> c.toString())
                    .reduce((acc, e) -> acc + e)
                    .get();
          }

          /**
           * Makes a byte list from a string
           */
          private List<Byte> stringToBytestring(String str) {
               List<Byte> res = new ArrayList<>();
               for(char c : str.toCharArray()) {
                    res.add((byte)c);
               }
               return res;
          }

          /**
           * read one line (delimited by \r\n) from a socket's inputstream
           */
          private List<Byte> readSocketLine(Socket s) {
               DataInputStream in = null;
               try {
                    in = new DataInputStream(
                         s.getInputStream());
               } catch (Exception e) {
                    System.err.println(e);
                    e.printStackTrace();
               }

               List<Byte> packetBytes = new ArrayList<>();
               while(true) {
                    try {
                         int lastIndex = packetBytes.size() - 1;
                         if(lastIndex >= 1 &&
                            packetBytes.get(lastIndex)     == '\n' &&
                            packetBytes.get(lastIndex - 1) == '\r') {
                              return packetBytes;
                         }

                         byte [] bl = new byte[1];
                         int bytesRead = in.read(bl);
                         if(bytesRead == -1) {
                              System.out.println("socket closed while reading");
                              break;
                         }
                         byte b = bl[0];
                         packetBytes.add(b);
                    } catch (Exception e) {
                         System.err.println(e);
                         e.printStackTrace();
                    }
               }
               return packetBytes; // if something goes wrong, return what we got until it did
          }

          /**
           * write a string to an outputstream
           */
          private void writeString(DataOutputStream stream, String str) {
               try {
                    for(Byte b : stringToBytestring(str)) {
                         stream.write(b);
                    }
                    stream.flush();
               } catch (Exception e) {
                    System.err.println(e);
                    e.printStackTrace();
               }
          }

          /**
           * try to match a regex to a line, replacing match and outputting to stream
           */
          private boolean rewriteLineRegex(String regex, String repl, DataOutputStream stream, String line) {
               boolean ret = false;
               Pattern pat = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
               Matcher match = pat.matcher(line);
               if(match.find()) {
                    writeString(stream, repl);
                    ret = true;
               }
               return ret;
          }
     }

     private class DNSCachePruner extends Thread {
          Map<String,InetAddress> map;
          Map<String,Instant> timeMap;
          public DNSCachePruner(Map<String,InetAddress> map, Map<String,Instant> timeMap) {
               this.map = map;
               this.timeMap = timeMap;
          }

          public void run() {
               while(true) {
                    for(String k : map.keySet()) {
                         Instant t = timeMap.get(k);
                         if(t.plus(30, ChronoUnit.SECONDS).isBefore(Instant.now())) {
                              System.out.println("pruning host: " + k);
                              map.remove(k);
                              timeMap.remove(k);
                         }
                    }
                    try {
                         Thread.sleep(1000);
                    } catch(InterruptedException e) {
                         System.err.println("dns cache pruner thread sleep interrupted");
                         System.err.println(e);
                         e.printStackTrace();
                    }
               }
          }
     }
}
